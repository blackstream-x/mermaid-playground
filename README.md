# Mermaid Playground

_playing around with [Mermaid](https://mermaid.js.org/) diagrams_

Official resources:
- [Documentation](https://mermaid.js.org/intro/)
- [Live editor](https://mermaid.live/)

## Contents

- [GitGraph](./git-graph.md)

---
Project image: cropped and scaled from [original image on Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Ellen_Price_as_the_Little_Mermaid,_Royal_Danish_Ballet,_1909.jpg) (public domain)
