# Git graph

[Official documentation](https://mermaid.js.org/syntax/gitgraph.html)

## First try

```mermaid
gitGraph TB:
    commit id: "Initial commit"
    branch dev
    commit id: "[dev1…]" type: HIGHLIGHT
    branch feature/new-stuff
    commit id: "awesome new stuff"
    checkout dev
    merge feature/new-stuff id: "MERGE feature/new-stuff → dev"
    checkout main
    merge dev id: "MERGE dev → main" tag: "v0.1.0"
    checkout dev
    commit id: "[dev2…]" type: HIGHLIGHT
    branch feature/brand-new
    commit id: "brand new stuff"
    checkout main
    commit id: "[main1…]" type: HIGHLIGHT
    branch hotfix/urgent
    commit id: "urgent production fix"
    checkout main
    merge hotfix/urgent id: "MERGE hotfix/urgent → main" tag: "v0.1.1"
    commit id: "[main2dev…]" type: HIGHLIGHT
    checkout dev
    merge main id: "RE-MERGE the urgent hotfix (main → dev)"
    checkout feature/brand-new
    commit id: "more additions"
    checkout dev
    merge feature/brand-new id: "MERGE feature/brand-new → dev"
```
